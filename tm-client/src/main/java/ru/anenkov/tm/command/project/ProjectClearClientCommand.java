package ru.anenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;

public class ProjectClearClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Project-clear";
    }

    @Override
    public @Nullable String description() {
        return "Clear projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[CLEAR PROJECTS]");
        bootstrap.getProjectEndpoint().toString();
        System.out.println("[CLEAR SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}