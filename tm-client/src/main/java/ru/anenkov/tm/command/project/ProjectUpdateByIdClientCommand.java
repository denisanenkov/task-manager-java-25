package ru.anenkov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.command.AbstractCommandClient;
import ru.anenkov.tm.enumeration.Role;
import ru.anenkov.tm.util.TerminalUtil;

public class ProjectUpdateByIdClientCommand extends AbstractCommandClient {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @Nullable String name() {
        return "Project-update-by-id";
    }

    @Override
    public @Nullable String description() {
        return "Update project - list by id";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME PROJECT: ");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION PROJECT: ");
        @NotNull final String description = TerminalUtil.nextLine();
        bootstrap.getProjectEndpoint().updateByIdProject(bootstrap.getSession(), id, name, description);
        System.out.println("[UPDATE SUCCESS]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN, Role.USER};
    }

}