package ru.anenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.ISessionRepository;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.entity.Session;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyListException;
import ru.anenkov.tm.exception.empty.EmptyUserIdException;

import java.util.List;

public final class SessionRepository extends AbstractRepository<Session, SessionDTO> implements ISessionRepository {

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> getListDTOs() {
        return entityManager.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class).getResultList();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> getListEntities() {
        return entityManager.createQuery("SELECT e FROM Session e", Session.class).getResultList();
    }


    @Override
    @SneakyThrows
    public void removeByUserId(@Nullable final String userId) {
        @Nullable final Session session = findSessionById(userId);
        if (session == null) throw new EmptyEntityException();
        removeSession(session);
    }

    @Override
    @SneakyThrows
    public void removeSession(@Nullable final Session session) {
        if (session == null) throw new EmptyEntityException();
        entityManager.remove(session);
    }

    @Override
    @SneakyThrows
    public @Nullable Session findSessionById(@Nullable final String id) {
        @Nullable final List<Session> listSessions = entityManager.createQuery("SELECT e FROM Session e WHERE e.id = :id", Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listSessions);
    }

    @Override
    @SneakyThrows
    public boolean isExists(@Nullable final String id) {
        @Nullable final Session session = findSessionById(id);
        if (session == null) return false;
        return true;
    }

    @Override
    @SneakyThrows
    public void clearAll() {
        @NotNull final List<Session> listSessions = getListEntities();
        for (@NotNull final Session session : listSessions) {
            entityManager.remove(session);
        }
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final List<Session> sessionList = entityManager.createQuery("SELECT e FROM Session e WHERE e.userId = :userId", Session.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList();
        return sessionList;
    }

    @Override
    @SneakyThrows
    public List<Session> getList() {
        return getListEntities();
    }

    @Override
    @SneakyThrows
    public void clearAllSessions() {
        @NotNull final List<Session> listSessions = getList();
        for (@NotNull final Session session : listSessions) {
            entityManager.remove(session);
        }
    }

}
