package ru.anenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.repository.IUserRepository;
import ru.anenkov.tm.dto.entitiesDTO.UserDTO;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.EmptyEntityException;
import ru.anenkov.tm.exception.empty.EmptyListException;

import java.util.List;

public final class UserRepository extends AbstractRepository<User, UserDTO> implements IUserRepository {

    @Override
    @Nullable
    @SneakyThrows
    public List<User> getListEntities() {
        return entityManager.createQuery("SELECT e FROM User e", User.class)
                .getResultList();
    }

    @Override
    @SneakyThrows
    public List<UserDTO> getListDTOs() {
        return entityManager.createQuery("SELECT e FROM UserDTO e", UserDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Long count() {
        @Nullable final Long count = entityManager.createQuery("SELECT COUNT(e) FROM User e", Long.class)
                .getSingleResult();
        return count;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByIdDTO(@NotNull final String id) {
        @Nullable final List<UserDTO> listUsers = entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.id = :id", UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return getFirstDTO(listUsers);
    }

    @Override
    @Nullable
    @SneakyThrows
    public User findByIdEntity(@NotNull final String id) {
        @Nullable final List<User> listUsers = entityManager.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listUsers);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLoginDTO(@NotNull final String login) {
        @Nullable final List<UserDTO> listUsers = entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        return getFirstDTO(listUsers);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLoginEntity(@NotNull final String login) {
        @Nullable final List<User> listUsers = entityManager.createQuery("SELECT e FROM User e WHERE e.login = :login", User.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listUsers);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmailDTO(@NotNull final String email) {
        @Nullable final List<UserDTO> listUsers = entityManager.createQuery("SELECT e FROM UserDTO e WHERE e.email = :email", UserDTO.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList();
        return getFirstDTO(listUsers);
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmailEntity(@NotNull final String email) {
        @Nullable final List<User> listUsers = entityManager.createQuery("SELECT e FROM User e WHERE e.email = :email", User.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList();
        return getFirstEntity(listUsers);
    }

    @Override
    @SneakyThrows
    public void removeByLogin(@NotNull final String login) {
        @Nullable final User user = findByLoginEntity(login);
        if (user == null) throw new EmptyEntityException();
        entityManager.remove(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeByEmail(@NotNull final String email) {
        @Nullable final User user = findByEmailEntity(email);
        if (user == null) throw new EmptyEntityException();
        entityManager.remove(user);
    }

    @Nullable
    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        @Nullable final User user = findByIdEntity(id);
        if (user == null) throw new EmptyEntityException();
        entityManager.remove(user);
    }

    @Override
    @Nullable
    @SneakyThrows
    public List<User> getList() {
        return entityManager.createQuery("SELECT e FROM User e", User.class).getResultList();
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final User user) {
        if (user == null) throw new EmptyEntityException();
        entityManager.remove(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User add(@NotNull final User user) {
        entityManager.persist(user);
        return user;
    }

    @Override
    @SneakyThrows
    public void clearEntities() {
        @NotNull final List<User> listUsers = getList();
        for (@NotNull final User user : listUsers) {
            entityManager.remove(user);
        }
    }

    @Override
    @SneakyThrows
    public void createUser(
            @NotNull final String login,
            @NotNull final String password
    ) {
        @NotNull final User user = new User(login, password);
        merge(user);
    }

}
