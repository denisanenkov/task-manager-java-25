package ru.anenkov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    @SneakyThrows
    void createWithDescription(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    void create(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    @SneakyThrows
    void add(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "session", partName = "session") final TaskDTO task
    );

    @WebMethod
    @SneakyThrows
    void remove(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task", partName = "task") final TaskDTO task
    );

    @WebMethod
    @SneakyThrows
    List<TaskDTO> findAll(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    void clear(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

    @WebMethod
    @SneakyThrows
    TaskDTO findOneByIndex(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    TaskDTO findOneByName(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name", partName = "name") final String name
    );

    @WebMethod
    @SneakyThrows
    TaskDTO findOneById(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull void removeOneByIndex(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "index", partName = "index") final Integer index
    );

    @WebMethod
    @SneakyThrows
    @NotNull void removeOneByName(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "name", partName = "name") @Nullable String name
    );

    @WebMethod
    @SneakyThrows
    @NotNull void removeOneById(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") final String id
    );

    @WebMethod
    @SneakyThrows
    @NotNull void updateTaskById(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @NotNull @WebParam(name = "id", partName = "id") final String id,
            @NotNull @WebParam(name = "name", partName = "name") final String name,
            @NotNull @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    @SneakyThrows
    void updateTaskByIndex(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    );

    @WebMethod
    void load(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task", partName = "task") final List<TaskDTO> tasks
    );

    @WebMethod
    List<TaskDTO> getList(
            @NotNull @WebParam(name = "session", partName = "session") final SessionDTO session
    );

}
