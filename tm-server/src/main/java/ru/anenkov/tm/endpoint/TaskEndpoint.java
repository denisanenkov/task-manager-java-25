package ru.anenkov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.endpoint.ITaskEndpoint;
import ru.anenkov.tm.api.service.IServiceLocator;
import ru.anenkov.tm.dto.entitiesDTO.SessionDTO;
import ru.anenkov.tm.dto.entitiesDTO.TaskDTO;
import ru.anenkov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    public TaskEndpoint() {
        super(null);
    }

    @WebMethod
    @SneakyThrows
    public void createWithDescription(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name, description);
    }

    @WebMethod
    @SneakyThrows
    public void create(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().create(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    public void add(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task") final TaskDTO task
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().add(session.getUserId(), task);
    }

    @WebMethod
    @SneakyThrows
    public void remove(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task") final TaskDTO task
    ) {
        serviceLocator.getSessionService().validate(session);
        Task newTask = serviceLocator.getTaskService().toTask(session.getUserId(), task);
        serviceLocator.getTaskService().remove(session.getUserId(), newTask);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public List<TaskDTO> findAll(
            @Nullable @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().toTaskDTOList
                (serviceLocator.getTaskService().getListEntities(session.getUserId()));
    }

    @WebMethod
    @SneakyThrows
    public void clear(
            @Nullable @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public TaskDTO findOneByIndex(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().toTaskDTO
                (serviceLocator.getTaskService().findOneByIndexEntity(session.getUserId(), index));
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public TaskDTO findOneByName(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().toTaskDTO
                (serviceLocator.getTaskService().findOneByNameEntity(session.getUserId(), name));
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public TaskDTO findOneById(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().toTaskDTO
                (serviceLocator.getTaskService().findOneByIdEntity(session.getUserId(), id));
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void removeOneByIndex(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeOneByIndex(session.getUserId(), index);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void removeOneByName(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeOneByName(session.getUserId(), name);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void removeOneById(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeOneById(session.getUserId(), id);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void updateTaskById(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateTaskById(session.getUserId(), id, name, description);
    }

    @WebMethod
    @SneakyThrows
    @Nullable
    public void updateTaskByIndex(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "id") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().updateTaskByIndex(session.getUserId(), index, name, description);
    }

    @WebMethod
    @SneakyThrows
    public void load(
            @Nullable @WebParam(name = "session") final SessionDTO session,
            @Nullable @WebParam(name = "task") final List<TaskDTO> tasks
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().load
                (serviceLocator.getTaskService().toTaskList(session.getUserId(), tasks));
    }

    @WebMethod
    @Nullable
    @SneakyThrows
    public List<TaskDTO> getList(
            @Nullable @WebParam(name = "session") final SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().toTaskDTOList
                (serviceLocator.getTaskService().getListEntities(session.getUserId()));
    }

}
