package ru.anenkov.tm.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.dto.entitiesDTO.Domain;

import java.io.Serializable;

@AllArgsConstructor
public final class DomainService implements IDomainService, Serializable {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IProjectService projectService;

    @Override
    @SneakyThrows
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTasks());
        projectService.load(domain.getProjects());
    }

    @Override
    @SneakyThrows
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setTasks(taskService.getList());
        domain.setProjects(projectService.getList());
        domain.setUsers(userService.getList());
    }

}
